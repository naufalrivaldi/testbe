@extends('app.master')

@section('content')
  <div class="row">
    <div class="col-md-3">
      <div class="card">
        <div class="card-header">
          New Product
        </div>
        <div class="card-body">
          <form>
            <div class="form-group">
              <label for="title_product">Title Product</label>
              <input type="text" name="title_product" class="form-control" id="title_product">
              
              <!-- error -->
              <small class="text-danger error_title_product"></small>
            </div>

            <div class="form-group">
              <label for="brands">Brands</label>
              <input type="text" name="brands" class="form-control" id="brands">
              
              <!-- error -->
              <small class="text-danger error_brands"></small>
            </div>

            <div class="form-group">
              <label for="gender">Gender</label>
              <select name="gender" id="gender" class="form-control">
                <option value="">-- Select Gender --</option>
                <option value="man">Man</option>
                <option value="woman">Woman</option>
              </select>
              
              <!-- error -->
              <small class="text-danger error_gender"></small>
            </div>

            <div class="form-group">
              <label for="category">Category</label>
              <select name="category" id="category" class="form-control">
                <option value="">-- Select Category --</option>
                @foreach($category as $ctr)
                  <option value="{{ $ctr->id }}">{{ $ctr->name }}</option>
                @endforeach
              </select>
              
              <!-- error -->
              <small class="text-danger error_category"></small>
            </div>

            <div class="form-group subcategory">

            </div>

            <div class="form-group">
              <label for="keterangan">Keterangan</label>
              <textarea name="keterangan" id="keterangan" class="form-control" rows="5"></textarea>
            </div>

            <button class="btn btn-primary btn-block text-center btn-submit">Simpan</button>
          </form>
        </div>
      </div>
    </div>

    <div class="col-md-9">
      <div class="card">
        <div class="card-header text-center">
          List Product
        </div>
        <div class="card-body">
          <table class="table table-bordered product" style="font-size:.7em">
            <thead>
              <tr>
                <th>No</th>
                <th>Title Product</th>
                <th>Brand</th>
                <th>Gender</th>
                <th>Category</th>
                <th>Sub Category</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  
@endsection

@section('modal')

@endsection

@push('javascript')
  <script>
    $(document).ready(function(){
      $('.product').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('product.index') }}",
        columns:[
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'title_product', name: 'title_product' },
          { data: 'brands', name: 'brands' },
          { data: 'gender', name: 'gender' },
          { data: 'categoryName', name: 'categoryName' },
          { data: 'subCategoryName', name: 'subCategoryName' },
          { data: 'action', name: 'action', orderable:false }
        ]
      });

      // view subcategory
      $('#category').on('change', function(){
        let categoryId = $(this).val();
        
        $.ajax({
          url: "{{ url('subcategory') }}"+'/'+categoryId,
          type: 'GET',
          success: function(data){
            let subCategory = $('.subcategory');
            subCategory.empty();
            subCategory.append(`
              <label for="subcategory">Sub Category</label>
              <select name="subcategory" id="subcategory" class="form-control">
              </select>
            `);
            $.each(data, function(){
              $('#subcategory').append(`<option value="`+this.id+`">`+this.name+`</option>`);
            });

            subCategory.append(`<small class="text-danger error_subcategory"></small>`);
          },
          error: function(){
						alert('Error!')
					}
        });
      });

      // simpan data
      $('.btn-submit').on('click', function(e){
        e.preventDefault();
        let title_product = $('#title_product').val();
        let brands = $('#brands').val();
        let gender = $('#gender').val();
        let category = $('#category').val();
        let subcategory = $('#subcategory').val();
        let keterangan = $('#keterangan').val();

        $.ajax({
          url: "{{ route('product.store') }}",
          type: 'POST',
          data: {
            '_token' : '{{ csrf_token() }}',
            'title_product' : title_product,
            'brands' : brands,
            'gender' : gender,
            'category' : category,
            'subcategory' : subcategory,
            'keterangan' : keterangan
          },
          success: function(data){
            if(data.success == false){
              sendValidator(data.message);
            }else{
              swal({
                title: "Success!",
                text: "Simpan data berhasil.",
                icon: "success",
                button: "OK",
              });
              let table = $('.product').DataTable();
              table.ajax.reload();
              
              // empty
              let subCategory = $('.subcategory');
              subCategory.empty();
              $('#title_product').val('');
              $('#brands').val('');
              $('#gender').val('');
              $('#category').val('');
              $('#keterangan').val('');
            }
          },
          error: function(){
						alert('Error!')
					}
        });
      });

      function sendValidator(message){
        let title_product = $('.error_title_product');
        let brands = $('.error_brands');
        let gender = $('.error_gender');
        let category = $('.error_category');
        let subcategory = $('.error_subcategory');

        title_product.empty();
        brands.empty();
        gender.empty();
        category.empty();
        subcategory.empty();

        if(message.title_product){
          title_product.append(message.title_product[0]);
        }
        if(message.brands){
          brands.append(message.brands[0]);
        }
        if(message.gender){
          gender.append(message.gender[0]);
        }
        if(message.category){
          category.append(message.category[0]);
        }
        if(message.subcategory){
          subcategory.append(message.subcategory[0]);
        }
      }
    });

    // view data
    $(document).on('click', '.view', function(){
      let id = $(this).data('id');

      $.ajax({
        url: "{{ url('product') }}"+'/'+id,
        type: 'GET',
        success: function(data){
          swal(data.title_product, data.keterangan);
        },
          error: function(){
						alert('Error!')
					}
      });
    });

    // delete data
    $(document).on('click', '.delete', function(){
      let id = $(this).data('id');

      swal({
        title: "Delete data product?",
        text: "Data akan terhapus secara permanen!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.ajax({
            url: "{{ url('product') }}"+'/'+id,
            type: 'DELETE',
            data: {
              '_token' : '{{ csrf_token() }}'
            },
            success: function(){
              let table = $('.product').DataTable();
              table.ajax.reload();
            },
            error: function(){
              alert('Error!')
            }
          });
        }
      });
    });
  </script>
@endpush
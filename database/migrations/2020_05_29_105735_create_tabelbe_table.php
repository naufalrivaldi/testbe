<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTabelbeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabelbe', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title_product', 100);
            $table->string('brands', 50);
            $table->enum('gender', ['man', 'woman']);
            $table->unsignedBigInteger('category');
            $table->unsignedBigInteger('subcategory');
            $table->text('keterangan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabelBE');
    }
}

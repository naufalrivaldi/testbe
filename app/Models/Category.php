<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    // fk
    public function product(){
        $this->hasMany('App\Models\TableBE', 'category');
    }

    public function subCategory(){
        $this->hasMany('App\Models\SubCategory', 'parent');
    }
}

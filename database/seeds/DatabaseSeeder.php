<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(categoryTableSeeder::class);
        $this->call(subCategoryTableSeeder::class);
        $this->call(tabelBETableSeeder::class);
    }
}

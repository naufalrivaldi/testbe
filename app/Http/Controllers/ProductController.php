<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Category;
use App\Models\SubCategory;
use App\Models\tabelBE as Product;

use DataTables;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['category'] = Category::all();
        if($request->ajax()){
            $arr = [];
            $no = 1;
            $product = Product::all();
            
            return datatables()->of($product)
                                ->addColumn('action', function($data){
                                    $button = '<button class="btn btn-primary btn-sm mr-2 view" data-id="'.$data->id.'">View</button>';
                                    $button .= '<button class="btn btn-danger btn-sm delete" data-id="'.$data->id.'">Delete</button>';

                                    return $button;
                                })
                                ->addColumn('categoryName', function($data){
                                    return $data->Category->name;
                                })
                                ->addColumn('subCategoryName', function($data){
                                    return $data->subCategory->name;
                                })
                                ->rawColumns(['action', 'categoryName', 'subCategoryName'])
                                ->addIndexColumn()
                                ->make(true);
        }

        return view('layouts.product.index', $data);
    }

    public function data(){
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'title_product' => 'required|min:4',
            'brands' => 'required|min:4',
            'gender' => 'required',
            'category' => 'required',
            'subcategory' => 'required'
        ], [
            'required' => 'Kolom ini tidak boleh kosong!',
            'min' => 'Minimal panjang karakter :min'
        ]);

        if($validate->fails()){
            return response()->json([
                'success' => false,
                'message' => $validate->getMessageBag()->toArray()
            ]);
        }else{
            Product::create([
                'title_product' => $request->title_product,
                'brands' => $request->brands,
                'gender' => $request->gender,
                'category' => $request->category,
                'subcategory' => $request->subcategory,
                'keterangan' => $request->keterangan
            ]);
            return response()->json(['success' => true]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Product::find($id);

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Product::find($id);
        $data->delete();
    }
}

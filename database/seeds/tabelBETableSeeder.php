<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class tabelBETableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for($i=0; $i<25; $i++){
            DB::table('tabelbe')->insert([
                'title_product' => $faker->name,
                'brands' => str_random(10),
                'gender' => $faker->randomElement(['man', 'woman']),
                'category' => rand(1,2),
                'subcategory' => rand(1,4),
                'keterangan' => $faker->text
            ]);
        }
    }
}

<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages(){
        return [
            'required' => 'Kolom ini tidak boleh kosong!',
            'min' => 'Minimal panjang karakter :min'
        ];
    }
    
    public function rules()
    {
        return [
            'title_product' => 'required|min:4',
            'brands' => 'required|min:4',
            'gender' => 'required',
            'category' => 'required',
            'subcategory' => 'required'
        ];
    }
}

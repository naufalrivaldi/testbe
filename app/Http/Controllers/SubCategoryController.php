<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\SubCategory;

class SubCategoryController extends Controller
{
    public function search($id){
        $data = SubCategory::where('parent', $id)->get();

        return response()->json($data);
    }
}

<!doctype html>
<html lang="en">
  <head>
    @include('page.head')
  </head>
  <body>
    <!-- navbar -->
    @include('page.navbar')
    <!-- navbar -->

    <!-- content -->
    <div class="container mt-3">
      @yield('content')
    </div>
    <!-- content -->

    <!-- modal -->
    @yield('modal')
    <!-- modal -->

    <!-- Optional JavaScript -->
    @include('page.javascript')
    @stack('javascript')
  </body>
</html>
<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class subCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subcategory')->insert([
            [
                'name' => 'Eiger',
                'parent' => 1
            ],
            [
                'name' => 'Off White',
                'parent' => 1
            ],
            [
                'name' => 'Jeans',
                'parent' => 2
            ],
            [
                'name' => 'Celana Pendek',
                'parent' => 2
            ]
        ]);
    }
}

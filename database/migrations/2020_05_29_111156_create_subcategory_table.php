<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubcategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcategory', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 50);
            $table->unsignedBigInteger('parent');

            // fk
            $table->foreign('parent')
                    ->references('id')
                    ->on('category')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });

        Schema::table('tabelbe', function(Blueprint $table){
            // fk
            $table->foreign('subcategory')
                    ->references('id')
                    ->on('subcategory')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcategory');
    }
}

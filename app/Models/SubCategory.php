<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'subcategory';
    protected $fillable = [
        'name', 'parent'
    ];

    public $timestamps = false;

    // fk
    public function product(){
        $this->hasMany('App\Models\TableBE', 'subcategory');
    }

    public function category(){
        $this->belongsTo('App\Models\Category', 'parent');
    }
}

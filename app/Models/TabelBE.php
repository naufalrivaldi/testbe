<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TabelBE extends Model
{
    protected $table = 'tabelbe';
    protected $fillable = [
        'title_product', 'brands', 'gender', 'category', 'subcategory', 'keterangan'
    ];

    public $timestamps = false;

    // fk
    public function Category(){
        return $this->belongsTo('App\Models\Category', 'category');
    }

    public function subCategory(){
        return $this->belongsTo('App\Models\SubCategory', 'subcategory');
    }
}
